<?php include_once "../config/connection.php"; 
    session_start();
    if (!isset($_SESSION["authenticated"]) || $_SESSION["authenticated"] !== true && $_SESSION["admin"] = false) {
        header("Location: ../login.php");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>admin</title>
</head>
<body>
    <h1>admin</h1>
    <h1>add/update/remove</h1>
    <form action="" method="post">
        <button name="add">add</button>
        <button name="update">update</button>
        <button name="remove">remove</button>
    </form>

    <?php
        if(isset($_POST['add'])) {
            header("Location: add.php");
        }
        if(isset($_POST['update'])) {
            header("Location: update.php");            
        }
        if(isset($_POST['remove'])) {
            header("Location: remove.php");
        }
    
    ?>
    


    <h1>companies:</h1>
    <form action="./admin_menu.php">
        <button>show</button>
    </form>
    
</body>
</html>