<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>praq2 act2</title>
    <link rel="stylesheet" type="text/css" href="./style.css">
    
</head>
<body>
    <form action="" method="post">
        <input type="number" name="number" placeholder="give num">
        <button name="button">initialize matrix</button>
    </form>
    <?php
    $m = [];
    $num = 1;
    if (isset($_POST["number"]) & $_POST["number"] != null){
        $num = $_POST["number"];
    }  
    $sum = 0;
    $mult = 1;
    if(isset($_POST["button"])){
        for ($i = 0; $i < 4 ; $i++) { 
            for ($j = 0; $j < 4; $j++) { 
                $m[$i][$j] = rand(10,100);
                $sum = $sum + $m[$i][$j];
                $mult = $mult * $m[$i][$j];
            }
        }
        // echo "<pre>";
        // print_r ($m);
        // echo "</pre>";
    }
    
    ?>
    <hr>
    <h3>matrix</h3>
    <table>
        <?php for ($i=0; $i < count($m); $i++): ?>
            <tr>
                <?php for ($j=0; $j < count($m[$i]); $j++):?>
                    <td><?php echo $m[$i][$j] ?></td>
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
    </table>
    <hr>
    <h3>above diag</h3>
    <table>
        <?php for ($i=0; $i < count($m); $i++): ?>
            <tr>
                <?php for ($j=0; $j < count($m[$i]); $j++):?>
                    <td>
                        <?php if($j > $i){echo $m[$i][$j];} ?>
                    </td>
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
    </table>
    <hr>
    <h3>mults of x</h3>
    <table>
        <?php for ($i=0; $i < count($m); $i++): ?>
            <tr>
                <?php for ($j=0; $j < count($m[$i]); $j++):?>
                    <td>
                        <?php if($m[$i][$j] % $num == 0){echo $m[$i][$j];} ?>
                    </td>
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
    </table>
    <hr>
    <h3>stat</h3>
    <table>
        <tr>
            <td>sum</td><td><?php echo $sum?></td>
        </tr>
        <tr>
            <td>mult</td><td><?php echo $mult?></td>
        </tr>
        <tr>
            <td>avg</td><td><?php echo ($sum / (count($m) * count($m[0]))) ?></td>
        </tr>
    </table>
    <hr>
    <h3>sum of memeber chiffres</h3>
    <table>
        <?php for ($i=0; $i < count($m); $i++): ?>
            <tr>
                <?php for ($j=0; $j < count($m[$i]); $j++):?>
                    <td>
                        <?php 
                            $str  = strval($m[$i][$j]);
                            $arr = str_split($str);
                            echo array_sum($arr);
                        ?>
                    </td>
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
    </table>
   


</body>
</html>