<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>home or index (by default)</title>
</head>
<body>
    <ul>
        <li><a href="web1">web1</a></li>
        <li><a href="web2">web2</a></li>
        <li><a href="?url=1">web3</a></li>
        <li><a href="?url=2">web4</a></li>
    </ul>
    <?php
    if(isset($_GET["url"]) && $_GET["url"]==1){
        echo "<h1>web3</h1>";
        
    }elseif(isset($_GET["url"]) && $_GET["url"]==2){
        echo "<h1>web4</h1>";
    }
    ?>
</body>
</html>