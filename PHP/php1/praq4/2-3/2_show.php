<?php
    $path = "root";
    $url = "<a href='?'>root</a>";
    if(isset($_GET['folder'])){
        $path .= "/".$_GET["folder"];
        $url .= "/<a href='?folder=".$_GET['folder']."'>".$_GET['folder']."</a>";
    }
    include "files.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Files And Folders</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        echo "<pre>";
        print_r(scandir("root"));
        echo "</pre>";
    ?>
    <div class="container">
        <div class="action">
            <h2>Create Folder</h2>
            <p><a href="show.php">Root</a></p>
            <form action="files.php" method="post">
                <input type="text" placeholder="folder name" name="folder_name">
                <input type="hidden" name="fetch" value="<?=$path?>">
                <br><br>
                <button name="c_folder">create folder</button>
            </form>
        </div>
        <div class="list">
            <h2>List of <?=$url?></h2>
            <?php
                for($i=2; $i<count(scandir($path)); $i++)
                {
            ?>
                <div class="folder-div">
                    <a href="?folder=<?=scandir("root")[$i]?>"><?=scandir($path)[$i]?> </a>
                </div>
            <?php
                }
            ?>
        </div>
    </div>
</body>
</html>