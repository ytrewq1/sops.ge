<?php  

function check_mail($mail){
    $adress = ["@gmail.com","@mail.ru"];
    $is_valid = false;
    foreach ($adress as $adres) {
        if (strpos($mail, $adres) !== false){
            $is_valid = true;
        }
    }
    return $is_valid;
}

function contains_numbers($password){
    $numbers = '/[0-9]/';
    $contains_num = false;
    if(preg_match($numbers,$password)){
        $contains_num = true;
    }
    return $contains_num;
}

function generate_code(){
    $n = rand(10000,99999);
    return (string)$n;
}

?>