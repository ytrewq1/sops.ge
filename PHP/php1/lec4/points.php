<?php
include "array.questions.php";
echo "<pre>";
print_r($_POST);
echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>points</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <div class="container">
        
        <h2><?=$_POST['name']." ".$_POST['surname']?></h2>
        <form action="./questions.php" method="post">
            <table class = "tb-question">
                <thead>
                    <tr>
                        <th>question</th>
                        <th>answer</th>
                        <th>estimation</th>
                        <th>point</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    for ($i=0; $i < count($questions) ; $i++) { 
                    ?>
                    <tr>
                        <td><?=$_POST['questions'][$i] ?></td>
                        <td><?=$_POST['answer'][$i] ?></td>
                        <th><?=$_POST['estimation'][$i] ?></th>
                        <td><?=$_POST['points'][$i] ?></td>
                    </tr>
                    <?php 
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan = 3>
                            <?php
                            $sum = 0;
                            $max = 0;
                            for ($i=0; $i < count($questions) ; $i++) { 
                                $sum = $sum + $_POST['estimation'][$i];
                                $max = $max + $_POST['points'][$i];

                            }
                            echo $sum." is your score out of ".$max;
                            ?>
                        </td>
                        <td>
                            <button>restart</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>

    </div>
    
</body>
</html>