<?php
include "array.questions.php";
echo "<pre>";
print_r($_POST);
echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>answers</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <div class="container">
        
        <h2><?=$_POST['name']." ".$_POST['surname']?></h2>
        <form action="./points.php" method="post">
            <table class = "tb-question">
                <thead>
                    <tr>
                        <th>question</th>
                        <th>answer</th>
                        <th>estimation</th>
                        <th>point</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    for ($i=0; $i < count($questions) ; $i++) {      ?>
                        <tr>
                            <td><?=$_POST['questions'][$i] ?>
                                <input type="hidden" name="questions[]" value="<?=$questions[$i]['question'] ?>">
                            </td>

                            <td><?=$_POST['answer'][$i] ?>
                                <input type="hidden" name="answer[]" value="<?=$_POST['answer'][$i] ?>">
                            </td>
                            <th><input type="number" name="estimation[]"></th>
                            <td><?=$_POST['points'][$i] ?>
                                <input type="hidden" name="points[]" value="<?= $questions[$i]['maxpoint'] ?>">
                            </td>
                        </tr>
                    <?php 
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan = 3>
                            <input type="hidden" name = "name" value="<?=$_POST['name']?>">
                            <input type="hidden" name = "surname" value="<?=$_POST['surname']?>">
                            
                        </td>
                        <td>
                            <button>submit</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>

    </div>
    
</body>
</html>

