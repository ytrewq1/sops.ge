<?php
include "array.questions.php";
shuffle($questions);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>questions</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <div class="container">
        <h2>questions</h2>
        <form action="./answers.php" method="post">
            <table class = "tb-question">
                <thead>
                    <tr>
                        <th>question</th>
                        <th>answer</th>
                        <th>point</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    for ($i=0; $i < count($questions) ; $i++) { 
                    ?>
                    <tr>
                        <td><?=$questions[$i]['question'] ?>
                            <input type="hidden" name="questions[]" value="<?=$questions[$i]['question'] ?>">
                        </td>
                        <?php   if ($questions[$i]['maxpoint'] == 1) {   ?>
                            <td>
                            <input type="radio" id="qa"  value='true' name="answer[]">
                            <label for="q1a">a) true</label><br>
                            <input type="radio" id="qb"  value='false' name="answer[]">
                            <label for="q1b">b) false</label><br>
                            </td>
                        <?php }else{ ?>
                            <td><textarea name="answer[]" id="res" cols="30" rows="5" ></textarea></td>
                        <?php }?>
                        
                        <td><?= $questions[$i]['maxpoint'] ?>
                        <input type="hidden" name="points[]" value="<?= $questions[$i]['maxpoint'] ?>">
                        </td>
                    </tr>
                    <?php 
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan = 2>
                            name: <input type="text" name = "name" required>
                            surname: <input type="text" name = "surname" required>
                        </td>
                        <td>
                            <button>submit</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>

    </div>  
</body>
</html>