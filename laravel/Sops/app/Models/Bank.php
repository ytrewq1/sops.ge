<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Bank extends Model
{
    use HasFactory;

    protected $fillable = [
        'bank_name',
    ];


    protected static function newFactory()
    {
        return \Database\Factories\BankFactory::new();
    }

    public function payments():HasMany
    {
        return $this->hasMany(Payment::class);
    }
}
