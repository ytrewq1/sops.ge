<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Gun extends Model
{
    use HasFactory;

    protected $fillable = [
        'gun_name',
        'category_id',
        'brand_id',
    ];


    protected static function newFactory()
    {
        return \Database\Factories\GunFactory::new();
    }

    public function bookings():HasMany
    {
        return $this->hasMany(Booking::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
