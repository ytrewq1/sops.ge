<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
        'user_id',
        'bank_id',
        'payment_time',
    ];


    protected static function newFactory()
    {
        return \Database\Factories\PaymentFactory::new();
    }

    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class);
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
