<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'gun_id',
        'tir_id',
        'bullets',
        'user_id',
        'time',
    ];

    protected static function newFactory()
    {
        return \Database\Factories\BookingFactory::new();
    }


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function gun(): BelongsTo
    {
        return $this->belongsTo(Gun::class);
    }
    public function tir(): BelongsTo
    {
        return $this->belongsTo(Tir::class);
    }
}
