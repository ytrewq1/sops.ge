<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tir extends Model
{
    use HasFactory;

    protected $fillable = [
        'room_name',
    ];


    protected static function newFactory()
    {
        return \Database\Factories\TirFactory::new();
    }

    public function bookings():HasMany
    {
        return $this->hasMany(Booking::class);
    }
}
