<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_name',
        'parent_category_id',
    ];

    /**
     * The factory instance for the model.
     *
     * @return \Database\Factories\CategoryFactory
     */
    protected static function newFactory()
    {
        return \Database\Factories\CategoryFactory::new();
    }


    public function guns():HasMany
    {
        return $this->hasMany(Gun::class);
    }
    public function products():HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function parent()
    {
        return $this->belongsTo(Product::class, );
    }

    public function children()
    {
        return $this->hasMany(Product::class);
    }
}
