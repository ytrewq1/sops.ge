<?php

namespace App\Http\Controllers;
use App\Models\Payment;
use App\Http\Requests\PaymentRequest;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function makePayment(PaymentRequest $request)
    {
        $payment = Payment::create($request->all());

        return response()->json(['message' => 'Payment successful', 'payment' => $payment], 201);
    }
}
