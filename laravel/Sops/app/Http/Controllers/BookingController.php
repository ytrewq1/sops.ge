<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Http\Requests\BookingRequest;
use App\Models\Booking;
class BookingController extends Controller
{
    public function makeBooking(BookingRequest $request)
    {
        $booking = Booking::create($request->all());

        return response()->json(['message' => 'Booking successful', 'booking' => $booking], 201);
    }
}
