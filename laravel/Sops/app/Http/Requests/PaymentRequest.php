<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'bank_id' => 'required|exists:banks,id',
            'amount' => 'required|numeric|min:1.00',
        ];
    }
    public function messages()
    {
        return [
            'user_id.exists' => 'The selected user does not exist.',
            'bank_id.exists' => 'The selected bank does not exist.',
        ];
    }
}
