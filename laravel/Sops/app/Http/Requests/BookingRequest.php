<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'gun_id' => 'required|exists:guns,id',
            'tir_room_id' => 'required|exists:tir_rooms,id',
            'time' => 'required|date',
            'bullets' => 'required|integer',
            'user_id' => 'required|exists:users,id',

        ];
    }

    public function messages()
    {
        return [
            'user_id.exists' => 'The selected user does not exist.',
            'gun_id.exists' => 'The selected gun does not exist.',
            'tir_room_id.exists' => 'The selected tir room does not exist.',
        ];
    }
}
