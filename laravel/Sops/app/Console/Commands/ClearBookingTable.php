<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearBookingTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:clear-booking-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $tableName = 'booking';

        $cutoffDate = Carbon::now()->subMonths(2);

        DB::table($tableName)->where('time', '<', $cutoffDate)->delete();

        $this->info('bookings older than 2 months have been cleared.');
    }
}
