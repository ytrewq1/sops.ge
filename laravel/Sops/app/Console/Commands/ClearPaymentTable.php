<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ClearPaymentTable extends Command
{

    protected $signature = 'app:clear-payment-table';
    protected $description = 'Clear old payments';

    public function handle()
    {
        $tableName = 'payment';

        $cutoffDate = Carbon::now()->subMonths(2);

        DB::table($tableName)->where('payment_time', '<', $cutoffDate)->delete();

        $this->info('Payments older than 2 months have been cleared.');

    }
}
