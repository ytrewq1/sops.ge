<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class RunMigrations extends Command
{

    protected $signature = 'migrate:ordered';
    protected $description = 'run migrations with defined order';


    public function handle()
    {
        $this->info('Running custom migrations...');

        Artisan::call('migrate --path=/database/migrations/2024_01_23_163116_create_tirs_table.php');
        Artisan::call('migrate --path=/database/migrations/2024_01_23_161829_create_banks_table.php');
        Artisan::call('migrate --path=/database/migrations/2024_01_23_162225_create_brands_table.php');
        Artisan::call('migrate --path=/database/migrations/2024_01_23_161951_create_categories_table.php');
        Artisan::call('migrate --path=/database/migrations/2014_10_12_000000_create_users_table.php');
        Artisan::call('migrate --path=/database/migrations/2024_01_23_163213_create_guns_table.php');
        Artisan::call('migrate --path=/database/migrations/2024_01_23_160947_create_products_table.php');
        Artisan::call('migrate --path=/database/migrations/2024_01_23_162432_create_payments_table.php');
        Artisan::call('migrate --path=/database/migrations/2024_01_23_163359_create_bookings_table.php');
        Artisan::call('migrate --path=/database/migrations/2024_01_23_164458_create_product_user_table.php');

        $this->info('Custom migrations completed.');
    }
}
