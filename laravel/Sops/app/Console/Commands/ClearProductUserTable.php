<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ClearProductUserTable extends Command
{

    protected $signature = 'app:clear-product-user-table';
    protected $description = 'Clear old purchases';

    public function handle()
    {
        $tableName = 'product_user';

        $cutoffDate = Carbon::now()->subMonths(2);

        DB::table($tableName)->where('purchase_time', '<', $cutoffDate)->delete();

        $this->info('Purchases older than 2 months have been cleared.');

    }
}
