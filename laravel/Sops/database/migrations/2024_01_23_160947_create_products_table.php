<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Brand;
use App\Models\Category;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            //$table->foreignId(Category::class)->constrained()->cascadeOnDelete();
            $table->foreignId('parent_category_id')->nullable()->constrained()->cascadeOnDelete();

            $table->decimal('price');
            $table->text('description');
            $table->foreignIdFor(Brand::class)->constrained()->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
