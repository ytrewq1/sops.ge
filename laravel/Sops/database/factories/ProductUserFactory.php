<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\ProductUser;

class ProductUserFactory extends Factory
{
    protected $model = ProductUser::class;

    public function definition()
    {
        return [
            'user_id' => function () {
                return \App\Models\User::factory()->create()->id;
            },
            'product_id' => function () {
                return \App\Models\Product::factory()->create()->id;
            },
            'payment_time' => $this->faker->dateTime(),

        ];
    }
}
