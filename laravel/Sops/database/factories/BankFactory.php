<?php

namespace Database\Factories;
use App\Models\Bank;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class BankFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Bank::class;

    public function definition(): array
    {
        return [
            'bank_name' => $this->faker->word,
        ];
    }
}
