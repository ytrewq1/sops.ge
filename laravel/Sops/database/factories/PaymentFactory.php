<?php

namespace Database\Factories;
use App\Models\Payment;

use Illuminate\Database\Eloquent\Factories\Factory;


class PaymentFactory extends Factory
{
    protected $model = Payment::class;

    public function definition(): array
    {
        return [
            'amount' => $this->faker->randomFloat(2, 1, 1000),
            'user_id' => function () {
                return \App\Models\User::factory()->create()->id;
            },
            'bank_id' => function () {
                return \App\Models\Bank::factory()->create()->id;
            },
            'payment_time' => $this->faker->dateTime(),

        ];
    }
}
