<?php

namespace Database\Factories;
use App\Models\Tir;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class TirFactory extends Factory
{
    protected $model = Tir::class;

    public function definition(): array
    {
        return [
            'room_name' => $this->faker->word,
        ];
    }
}
