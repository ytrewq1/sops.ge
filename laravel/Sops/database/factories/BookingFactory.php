<?php

namespace Database\Factories;
use App\Models\Booking;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class BookingFactory extends Factory
{
    protected $model = Booking::class;

    public function definition()
    {
        return [
            'gun_id' => function () {
                return \App\Models\Gun::factory()->create()->id;
            },
            'tir_id' => function () {
                return \App\Models\Tir::factory()->create()->id;
            },
            'bullets' => $this->faker->randomNumber(),
            'user_id' => function () {
                return \App\Models\User::factory()->create()->id;
            },
            'time' => $this->faker->dateTime(),
        ];
    }
}
