<?php

namespace Database\Factories;
use App\Models\Gun;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Gun>
 */
class GunFactory extends Factory
{
    protected $model = Gun::class;

    public function definition(): array
    {
        return [
            'gun_name' => $this->faker->word,
            'category_id' => function () {
                // You might want to replace this with logic to get a valid category ID
                return \App\Models\Category::factory()->create()->id;
            },
            'brand_id' => function () {
                // You might want to replace this with logic to get a valid brand ID
                return \App\Models\Brand::factory()->create()->id;
            },
        ];
    }
}
