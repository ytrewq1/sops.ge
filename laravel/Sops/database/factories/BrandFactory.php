<?php

namespace Database\Factories;
use App\Models\Brand;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class BrandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Brand::class;

    public function definition(): array
    {
        return [
            'brand_name' => $this->faker->word,
            'description' => $this->faker->paragraph,
        ];
    }
}
