<?php

namespace Database\Seeders;
use App\Models\Tir;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Tir::factory()->count(10)->create();
    }
}
