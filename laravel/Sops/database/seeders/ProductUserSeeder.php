<?php

namespace Database\Seeders;
use App\Models\ProductUser;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ProductUser::factory()->count(10)->create();

    }
}
