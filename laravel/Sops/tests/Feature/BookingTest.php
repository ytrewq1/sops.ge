<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Gun;
use App\Models\Tir;

class BookingTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_make_booking()
    {
        $user = User::factory()->create();
        $gun = Gun::factory()->create();
        $tir = Tir::factory()->create();

        $bookingData = [
            'gun_id' => $gun->id,
            'tir_room_id' => $tir->id,
            'date' => $this->faker->date,
            'bullets' => $this->faker->randomNumber(),
            'user_id' => $user->id,

        ];

        $response = $this->json('POST', '/make-booking', $bookingData);

        $response->assertStatus(201)
            ->assertJson([
                'message' => 'Booking successful',
            ]);

        $this->assertDatabaseHas('bookings', $bookingData);
    }
}
