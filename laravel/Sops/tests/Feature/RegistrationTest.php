<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class RegistrationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_it_registers_user()
    {
        $userData = [
            'firstname' => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'password' => $this->faker->password,
        ];

        $response = $this->json('POST', '/register', $userData);

        $response->assertStatus(201)
            ->assertJson([
                'message' => 'User registered successfully',
            ]);
        $this->assertDatabaseHas('users', [
            'username' => $userData['username'],
        ]);
    }

    public function test_user_registration_unique_username_failure()
    {
        $userData = [
            'firstname' => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'password' => $this->faker->password,
        ];

        $response = $this->json('POST', '/register', $userData);

        $response->assertStatus(422)
            ->assertJsonValidationErrors(['username']);
    }
}
