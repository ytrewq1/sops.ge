<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Bank;
use App\Models\Payment;

class PaymentTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_make_payment()
    {
        $user = User::factory()->create();
        $bank = Bank::factory()->create();

        $paymentData = [
            'user_id' => $user->id,
            'bank_id' => $bank->id,
            'amount' => $this->faker->randomFloat(2, 1, 1000),
        ];

        $response = $this->json('POST', '/make-payment', $paymentData);

        $response->assertStatus(201)
            ->assertJson([
                'message' => 'Payment successful',
            ]);

        $this->assertDatabaseHas('payments', [
            'user_id' => $user->id,
            'bank_id' => $bank->id,
            'amount' => $paymentData['amount'],
        ]);
    }
}
