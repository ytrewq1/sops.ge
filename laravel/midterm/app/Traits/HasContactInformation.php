<?php

namespace App\Traits;

trait HasContactInformation
{
    public string $mobile;
    public string $email;

    public function setMobile($mobile , $email){

        $this->mobile = $mobile;
        $this->email = $email;

        
    }

}