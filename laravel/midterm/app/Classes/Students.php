<?php

namespace App\Classes;

use App\Traits\HasContactInformation;
use Faker\Factory as Faker;
use Illuminate\Notifications\Notifiable;


class Students
{
    use HasContactInformation;
    use Notifiable;

    protected string $firstName;
    protected string $lastName;
    protected string $birthday;
    protected array $courses = [];




  

    public function __construct(string $firstName, string $lastName, string $birthday, array $courses, string $email)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthday = $birthday;
        $this->courses = $courses;
        $this->email = $email;


    }

    public static function GenerateStudent()
    {
        $faker = Faker::create();

        $firstName = $faker->firstname;
        $lastName = $faker->lastname;
        $birthday = $faker->date;
        $courses = [
            'Math',
            'Physics'
        ];
        $email = $faker->email;

        return [
            'firstName' => $firstName,
            'lastName' => $lastName,
            'birthday' => $birthday,
            'courses' => $courses,
            'email' => $email,
        ];

    }
    public function routeNotificationFor($driver, $notification = null): string
    {
        return $this->email;
    }

}