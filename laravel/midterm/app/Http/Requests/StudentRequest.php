<?php

namespace App\Http\Requests;
use Exception;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'firstName' => 'required|string',
            'lastName' => 'required|string',
            'birthday' => 'required|date',
            
            'courses' => 'array',
            'courses.*' => 'required|string',

            'emergency' => 'array',
            'emergency.*' => 'required|array',
            'emergency.*.name' => 'required|string',
            'emergency.*.mobile' => 'required|string',

            'email' => 'required|email'
        ];
    }


    protected function prepareForValidation(): void
    {
        $this->merge([
            'email' => strtolower($this->get('firstName')) . $this->get('lastName') . '@gau.edu.ge'
        ]);

        if (typeOf('firstName') != 'string' || typeOf('lastName') != 'string') {

            throw new Exception('სტუდენტის სახელი ან გვარი არასწორია'); 
        }
        if (typeOf('birthday') != 'date' ) {

            throw new Exception('დაბადების დღის თარიღი არასწორია'); 
        }
    }
}
