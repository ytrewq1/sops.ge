<?php

namespace App\Http\Controllers;

use App\Classes\Students;
use App\Http\Requests\StudentRequest;
use App\Notifications\Birthday;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Storage;


class StudentController2 extends Controller
{

    public function store(StudentRequest $request){
        $data = Students::GenerateStudent();

        $student = new Students(
            $data['firstName'],
            $data['lastName'],
            $data['birthday'],
            $data['courses'],
            $data['email'],
    
        );

        if($data['birthday'] == Carbon::now()){
            $student->notify(
                new Birthday($data['firstName'],  $data['lastName'])
            );
        }
        Storage::read('students.txt');

       
        return [
            'response' => 'Success!',
            $data,
        ];

        
    }

    
}