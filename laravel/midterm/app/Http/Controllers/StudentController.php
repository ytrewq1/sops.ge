<?php

namespace App\Http\Controllers;

use App\Classes\Students;
use App\Http\Requests\StudentRequest;
use App\Notifications\Birthday;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class StudentController extends Controller
{

    public function store(StudentRequest $request){
        $data = $request->validated();

        $student = new Students(
            $data['firstName'],
            $data['lastName'],
            $data['birthday'],
            $data['courses'],
            $data['email'],
    
        );

        
       
       
        return [
            'response' => 'Success!',
            $data,
        ];

        
    }

    
}