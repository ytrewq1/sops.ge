<?php

namespace App\Console\Commands;
use App\Classes\Students;
use App\Notifications\Birthday;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $stdata = Students::GenerateStudent();
        $student = new Students(
                            $stdata['firstName'],
                            $stdata['lastName'],
                            $stdata['birthday'],
                            $stdata['courses'],
                            $stdata['email'],
                            
        );
        if (Carbon::now() == $stdata['birthday']) {
            $student->notify(
                new Birthday($stdata['firstName'],  $stdata['lastName'])
            );
        }
    }

}
