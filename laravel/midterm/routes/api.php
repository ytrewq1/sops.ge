<?php

use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudentController2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['prefix' => '/students'], function (){
    Route::post('/cont', [StudentController::class, 'store']);
    Route::post('/cont2', [StudentController2::class, 'store']);

});
